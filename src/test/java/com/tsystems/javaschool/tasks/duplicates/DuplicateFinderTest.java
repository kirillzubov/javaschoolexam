package com.tsystems.javaschool.tasks.duplicates;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class DuplicateFinderTest {

    private DuplicateFinder duplicateFinder = new DuplicateFinder();

    @Test(expected = IllegalArgumentException.class)
    public void test() {
        //run
        duplicateFinder.process(null, new File("a.txt"));

        //assert : exception
    }

    @Test(expected = IllegalArgumentException.class)
    public void test1() {
        //run
        duplicateFinder.process(new File("a.txt"), null);

        //assert : exception
    }

    @Test
    public void test2() throws IOException {
        duplicateFinder.str.add("ccc");
        duplicateFinder.str.add("ddd");
        duplicateFinder.str.add("bbb");
        duplicateFinder.str.add("ddd");
        duplicateFinder.str.add("ddd");
        duplicateFinder.str.add("aaa");
        duplicateFinder.str.add("cdc");
        String[] expectedResult = {"aaa[1]", "bbb[1]", "ccc[1]", "cdc[1]", "ddd[3]"};
        duplicateFinder.dublicate();

        Assert.assertArrayEquals(duplicateFinder.str.toArray(), expectedResult);

        //assert : exception
    }


}