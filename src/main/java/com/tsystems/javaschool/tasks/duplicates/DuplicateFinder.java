package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.util.*;

class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     * <p>
     * // * @param sourceFile file to be processed
     * // * @param targetFile output file; append if file exist, create if not.
     *
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */

    List<String> str = new ArrayList<>();

    boolean process(File sourceFile, File targetFile) {
        if (sourceFile == null || targetFile == null) {
            throw new IllegalArgumentException();
        }
        try {
            DuplicateFinder df = new DuplicateFinder();
            df.read(sourceFile.getName());
            df.dublicate();
            df.write(targetFile.getName());
            return true;
        } catch (IOException e) {
            return false;
        }

    }


    private void read(String source) throws IOException {
        File file = new File(source);
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            str.add(line);
        }
        fileReader.close();

    }

    void dublicate() {
        Map<String, Integer> m = new HashMap<>();
        for (String a : str) {
            Integer freq = m.get(a);
            m.put(a, (freq == null) ? 1 : freq + 1);
        }
        str.clear();
        for (String key : m.keySet()) {
            str.add(key + "[" + m.get(key) + "]");
        }
        Collections.sort(str);
    }

    private void write(String target) throws IOException {

        File fout = new File(target);
        FileOutputStream fos = new FileOutputStream(fout);
        OutputStreamWriter osw = new OutputStreamWriter(fos);

        for (String s : str) {
            osw.write(s + '\n');
        }

        osw.close();
    }


}
